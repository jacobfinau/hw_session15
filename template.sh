# template.sh - edit this file
function usage
{
  echo "usage: $0 arguments ..."
  if [ $# -eq 1 ]
  then echo "ERROR: $1"
  fi
}

# Your script starts after this line
if [ $# -eq 0 ]
then
   usage
else
   echo "Jacob Finau"
   date
   echo ""
fi 

for i in $*; do 
  case $i in
    now)
     echo "It is now $(date +"%r")"
     echo "*****"
        ;;
    TestError)
     usage "TestError found"
     echo "*****"
        ;;
    *)
     usage "Do not know what to do with $i"
     echo "*****"
  esac
done
